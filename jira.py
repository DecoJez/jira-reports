import configparser
import os
import subprocess
from collections import OrderedDict
from datetime import date
from datetime import timedelta

import requests
from openpyxl import Workbook
from openpyxl.styles import Font


class cd:
    def __init__(self, newPath):
        self.newPath = os.path.expanduser(newPath)

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)


class JiraWorklogExporter(object):
    """Main class of Jira Worklog """
    def __init__(self, key, dateFrom, dateTo):
        """Inicjacja klasy
            Args:
                key (string): key of project
                dateFrom (string): date from
                dateTo (string): date to
            """
        super(JiraWorklogExporter, self).__init__()
        config = configparser.ConfigParser()
        config.read('config.ini')
        self.username = config['jira']['Username']
        self.password = config['jira']['Password']
        self.tasksList = []
        self.projectKey = key
        self.projectDateStart = dateFrom
        self.projectDateEnd = dateTo
        self.findWorklogs()
        dirName = "reports_{0}_{1}".format(self.projectDateStart, self.projectDateEnd)
        subprocess.call(["mkdir", dirName], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        if self.tasksList:
            with cd(dirName):
                self.saveSpreadsheet()


    def connectToApi(self, url):
        """Connection to API
            Args:
                url (sting): API URL
            Returns:
                json with results"""
        return requests.get(url, auth=(self.username, self.password)).json()

    def findWorklogs(self):
        """Find all worklogs, even that """
        projectDateStartWider = date(*map(int, self.projectDateStart.split('-'))) - timedelta(days=1)
        projectDateEndWider = date(*map(int, self.projectDateEnd.split('-'))) + timedelta(days=1)
        url = "https://xxxx.atlassian.net/rest/api/2/search?maxResults=1000&fields=*all&jql=project={0} and worklogDate > {1} and worklogDate < {2}".format(self.projectKey, projectDateStartWider, projectDateEndWider)
        response = self.connectToApi(url)
        if 'issues' in response:
            issues = response['issues']
            for issue in issues:
                self.calculateIssue(issue)

    def calculateIssue(self, issue):
        """Calculate time of task
            Args:
                issue (array): issue of project
                """
        issueWorklog = issue['fields']['worklog']
        task = OrderedDict()
        task['key'] = issue['key']
        task['summary'] = issue['fields']['summary']
        print(issue['key'])
        if issueWorklog['total'] <= issueWorklog['maxResults']:
            cleanedWorklog = self.removeTrashes(issueWorklog['worklogs'])
            # task['time'] = self.formatSecondsToHours(self.sumWorklogs(cleanedWorklog))
            task['billed'] = self.formatSecondsToHours(self.getBilled(cleanedWorklog))
        else:
            taskWorklogsUrl = "https://xxxx.atlassian.net/rest/api/2/issue/{0}/worklog".format(issue['key'])
            extandedIssue = self.connectToApi(taskWorklogsUrl)
            inDateWorklogsArray = self.removeTrashes(extandedIssue['worklogs'])
            # task['time'] = self.formatSecondsToHours(self.sumWorklogs(inDateWorklogsArray))
            task['billed'] = self.formatSecondsToHours(self.getBilled(inDateWorklogsArray))
        self.tasksList.append(task)

    def getBilled(self, worklogs):
        """Get billed from external API
            Args:
                worklogs (array): worklogs
                """
        sumBilledSeconds = 0
        for worklog in worklogs:
            billedUrl = "http://light4website.atlassian.net/rest/tempo-timesheets/3/worklogs/{0}".format(worklog['id'])
            worklogWithBilled = self.connectToApi(billedUrl)
            try:
                sumBilledSeconds += worklogWithBilled['billedSeconds']
            except KeyError:
                print('Worklog zawiera billed = 0')
        return sumBilledSeconds

    def removeTrashes(self, worklogs):
        """Clean worklogs to get only from right time
            Args:
                worklogs (array): worklogs to clean
                """
        projectDateStart = date(*map(int, self.projectDateStart.split('-')))
        projectDateEnd = date(*map(int, self.projectDateEnd.split('-')))
        inDateWorklogsArray = []
        for worklog in worklogs:
            worklogDate = date(*map(int, worklog['started'].split('T', 1)[0].split('-')))
            if projectDateStart <= worklogDate <= projectDateEnd:
                inDateWorklogsArray.append(worklog)
        return inDateWorklogsArray

    def sumWorklogs(self, worklogs):
        """Sum worklog for tasks
            Args:int
                worklogs (array): worklogs of task
            Returns:
                time (int): time of all worklogs in seconds
                """
        sumInSeconds = 0
        for worklog in worklogs:
            sumInSeconds += worklog['timeSpentSeconds']
        return sumInSeconds

    def sumProject(self, type):
        """Sum tasks for whole project
            Args:
                type (sting): type of parameter to sum
            Returns:

                """
        summary = 0
        for task in self.tasksList:
            summary += task[type]
        return round(summary, 2)

    def formatSecondsToHours(self, seconds):
        """Format time to JIRA format
            Args:
                seconds (int): amount of time in seconds
            Returns:
                hours (int): formated time
                """
        minutes = round(seconds/60, 0)
        hours = round(minutes/60, 2)
        return hours

    def saveSpreadsheet(self):
        """Save data to spreasheet"""
        wb = Workbook()
        dest_filename = '{0}_{1}_{2}_timesheet.xlsx'.format(self.projectKey, self.projectDateStart, self.projectDateEnd)
        ws1 = wb.active
        ws1.title = '{0}_timesheet'.format(self.projectKey)
        ft = Font(bold=True)
        ws1['A1'].value = 'Task key'
        ws1['B1'].value = 'Task description'
        ws1['C1'].value = 'Time'
        # ws1['E2'].value = 'Billed'
        ws1['A1'].font = ft
        ws1['B1'].font = ft
        ws1['C1'].font = ft
        max_row = 0
        for row, task in enumerate(self.tasksList):
            col = 0
            max_row = row
            for key, value in task.items():
                col += 1
                ws1.cell(column=col, row=row+2).value = value
        ws1.cell(column=1, row=max_row+3).value = 'Total'
        ws1.cell(column=1, row=max_row+3).font = ft
        ws1.cell(column=col, row=max_row+3).value = self.sumProject('billed')
        wb.save(dest_filename)
