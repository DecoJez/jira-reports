import configparser
import requests

from flask import Flask, render_template, g, request

from jira import JiraWorklogExporter


app = Flask(__name__)

@app.route("/")
def index():
    """Strona główna + pobieranie z API JIRY kluczy wszystkich projektów"""
    url = "https://xxxx.atlassian.net/rest/api/2/project"
    config = configparser.ConfigParser()
    config.read('config.ini')
    username = config['jira']['Username']
    password = config['jira']['Password']
    projects = requests.get(url, auth=(username, password)).json()
    keys = [project['key'] for project in projects]
    return render_template('index.html', keys=keys)

@app.route("/getTime", methods=["POST"])
def all():
    """Zapytanie pobierające dane z form-a oraz wykonujące jiraWorklogExporter"""
    dateFrom = request.form.get('dateFrom')
    dateTo = request.form.get('dateTo')
    keys = request.form.getlist('key')
    for key in keys:
        jiraWorklogExporter = JiraWorklogExporter(key, dateFrom, dateTo)
    return 'Done'

if __name__ == "__main__":
    app.run()
